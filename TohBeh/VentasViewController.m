//
//  VentasViewController.m
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import "VentasViewController.h"
#import "CoreDataAppDelegate.h"
#import "TiposVentas.h"
#import "Ventas.h"

@interface VentasViewController ()

@property CoreDataAppDelegate *appDelegate;
@property NSManagedObjectContext *context;

@property Ventas *venta;
@property TiposVentas *tipoVenta;

@end

@implementation VentasViewController

@synthesize appDelegate, context, venta, tipoVenta;

@synthesize nombreTourField, nombreClienteField, hotelField, numeroAdultosField, numeroNinosField, emailField, telefonoField;
@synthesize montoTotalField;
@synthesize fechaDisponiblePicker;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    context = [appDelegate managedObjectContext];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)agregar:(id)sender {
    
    venta = [NSEntityDescription insertNewObjectForEntityForName:@"Ventas" inManagedObjectContext:context];
    
    NSDate *aDate = [NSDate date];
    NSDateFormatter *fechaFormateada = [[NSDateFormatter alloc] init];
    [fechaFormateada setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    venta.fechaVenta = [fechaFormateada stringFromDate:aDate];
    
    venta.nombreTour = nombreTourField.text;
    venta.nombreCliente = nombreClienteField.text;
    venta.nombreHotel = hotelField.text;
    venta.correoElectronico = emailField.text;
    venta.telefono = telefonoField.text;
    venta.numeroAdultos = [NSNumber numberWithInteger:[numeroAdultosField.text intValue]];
    venta.numeroNinos = [NSNumber numberWithInteger:[numeroNinosField.text intValue]];
    venta.fechaTour = fechaDisponiblePicker.date;
    venta.montoTotal = montoTotalField.text;
    
    NSLog(@"total: %@", montoTotalField.text);
    
    [fechaFormateada setDateFormat:@"YYYY-MM-dd"];
    
    NSLog(@"Fecha elegida : %@ ", [fechaFormateada stringFromDate:fechaDisponiblePicker.date]);
    
    tipoVenta = [NSEntityDescription insertNewObjectForEntityForName:@"TiposVentas" inManagedObjectContext:context];
    tipoVenta.nombre = @"Por medio de App";
    
    venta.tipoVenta = tipoVenta;
    
    NSError *error;
    
    [context save:&error];
    UIAlertView *alert;
    
    if (!error) {
        NSLog(@"Oks: %@", @"el tour se pudo insertar");
        alert =[[UIAlertView alloc ] initWithTitle:@"Venta exitosa" message:@"Su Tour ha sido guardado" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
    } else {
        alert =[[UIAlertView alloc ] initWithTitle:@"Error" message:@"No se pudo agregar su venta" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    }
    [alert show];
}
@end
