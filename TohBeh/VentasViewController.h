//
//  VentasViewController.h
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VentasViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nombreTourField;
@property (weak, nonatomic) IBOutlet UITextField *nombreClienteField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *hotelField;
@property (weak, nonatomic) IBOutlet UITextField *telefonoField;

@property (weak, nonatomic) IBOutlet UITextField *numeroAdultosField;
@property (weak, nonatomic) IBOutlet UITextField *numeroNinosField;

@property (weak, nonatomic) IBOutlet UITextField *montoTotalField;

@property (weak, nonatomic) IBOutlet UIDatePicker *fechaDisponiblePicker;

- (IBAction)agregar:(id)sender;
@end
