//
//  Ventas.m
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import "Ventas.h"
#import "TiposVentas.h"


@implementation Ventas

@dynamic activo;
@dynamic correoElectronico;
@dynamic fechaTour;
@dynamic fechaVenta;
@dynamic nombreCliente;
@dynamic nombreHotel;
@dynamic nombreTour;
@dynamic numeroAdultos;
@dynamic numeroNinos;
@dynamic telefono;
@dynamic montoTotal;
@dynamic tipoVenta;

@end
