//
//  TiposVentas.h
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ventas;

@interface TiposVentas : NSManagedObject

@property (nonatomic, retain) NSString * nombre;
@property (nonatomic, retain) NSSet *venta;
@end

@interface TiposVentas (CoreDataGeneratedAccessors)

- (void)addVentaObject:(Ventas *)value;
- (void)removeVentaObject:(Ventas *)value;
- (void)addVenta:(NSSet *)values;
- (void)removeVenta:(NSSet *)values;

@end
