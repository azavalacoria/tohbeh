//
//  main.m
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CoreDataAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CoreDataAppDelegate class]));
    }
}
