//
//  Ventas.h
//  TohBeh
//
//  Created by Adrián Zavala Coria on 11/05/14.
//  Copyright (c) 2014 Hours. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TiposVentas;

@interface Ventas : NSManagedObject

@property (nonatomic, retain) NSNumber * activo;
@property (nonatomic, retain) NSString * correoElectronico;
@property (nonatomic, retain) NSDate * fechaTour;
@property (nonatomic, retain) NSString * fechaVenta;
@property (nonatomic, retain) NSString * nombreCliente;
@property (nonatomic, retain) NSString * nombreHotel;
@property (nonatomic, retain) NSString * nombreTour;
@property (nonatomic, retain) NSNumber * numeroAdultos;
@property (nonatomic, retain) NSNumber * numeroNinos;
@property (nonatomic, retain) NSString * telefono;
@property (nonatomic, retain) NSString * montoTotal;
@property (nonatomic, retain) TiposVentas *tipoVenta;

@end
